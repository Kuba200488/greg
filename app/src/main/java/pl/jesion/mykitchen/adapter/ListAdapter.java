package pl.jesion.mykitchen.adapter;

import java.util.List;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import pl.jesion.mykitchen.R;
import pl.jesion.mykitchen.util.DownloadPostImage;
import pl.jesion.mykitchen.util.TimeUtil;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder>{

    Context context;
    List<ListItem> items;
    Listener listener;


    public ListAdapter(Context context, List<ListItem> items, Listener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    public Context getContext() {
        return context;
    }

    @Override
    public ViewHolder onCreateViewHolder(
            final ViewGroup parent,
            final int viewType
    ) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item, parent, false);
        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(
            final ViewHolder holder,
            final int position
    ) {
        final ListItem item = items.get(position);
        holder.clockIv.setColorFilter(ContextCompat.getColor(context, item.isRunning() ? R.color.colorPrimary : R.color.gray));
        holder.clockIvCollapsed.setColorFilter(ContextCompat.getColor(context, item.isRunning() ? R.color.colorPrimary : R.color.gray));
        holder.descriptionTv.setText(item.getDescription());
        holder.nameTv.setText(item.getName());
        holder.nameTvCollapsed.setText(item.getName());
        holder.favoriteIv.setImageDrawable(ContextCompat.getDrawable(context, item.isFavorite() ? R.drawable.ic_star_selected : R.drawable.ic_star_regular));
        holder.favoriteIvCollapsed.setImageDrawable(ContextCompat.getDrawable(context, item.isFavorite() ? R.drawable.ic_star_selected : R.drawable.ic_star_regular));
        holder.timeLeftTv.setText(TimeUtil.getFormatedTime(item.getTimeLeft()));
        holder.timeLeftTv.setTextColor(ContextCompat.getColor(context, item.isRunning() ? R.color.colorPrimary : R.color.gray));

        holder.collapsedLayout.setVisibility(item.isExpanded() ? View.GONE : View.VISIBLE);
        holder.expandedLayout.setVisibility(item.isExpanded() ? View.VISIBLE : View.GONE);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                if(item.getTimeLeft() == 0 && item.isExpanded()) {
                    item.setTimeLeft(item.getTime());
                    onBindViewHolder(holder, position);
                } else {
                    item.toggle();
                    holder.collapsedLayout.setVisibility(item.isExpanded() ? View.GONE : View.VISIBLE);
                    holder.expandedLayout.setVisibility(item.isExpanded() ? View.VISIBLE : View.GONE);

                    if (item.isExpanded()) {
                        new DownloadPostImage(holder.photo).execute(item.getImageUrl());
                    }
                }
            }
        });

        holder.clockIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                holder.clockIv.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary));
                holder.clockIvCollapsed.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary));
                holder.timeLeftTv.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
                listener.onClockClicked(position);
            }
        });

        if (item.isExpanded()) {
            new DownloadPostImage(holder.photo).execute(item.getImageUrl());
        }

        holder.itemView.setBackgroundColor(ContextCompat.getColor(context, item.getTimeLeft() == 0 ? R.color.colorAccent : R.color.colorBright));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView nameTv;
        TextView nameTvCollapsed;
        ImageView favoriteIv;
        ImageView favoriteIvCollapsed;
        ImageView clockIv;
        ImageView clockIvCollapsed;
        TextView descriptionTv;
        TextView timeLeftTv;
        ImageView photo;

        LinearLayout expandedLayout;
        LinearLayout collapsedLayout;

        public ViewHolder(View v) {
            super(v);

            nameTv = (TextView) v.findViewById(R.id.item_name);
            nameTvCollapsed = (TextView) v.findViewById(R.id.item_name_collapsed);
            favoriteIv = (ImageView) v.findViewById(R.id.star);
            favoriteIvCollapsed = (ImageView) v.findViewById(R.id.star_collapsed);
            clockIv = (ImageView) v.findViewById(R.id.clock);
            clockIvCollapsed = (ImageView) v.findViewById(R.id.clock_collapsed);
            descriptionTv = (TextView) v.findViewById(R.id.item_description);
            timeLeftTv = (TextView) v.findViewById(R.id.time_left);
            photo = (ImageView) v.findViewById(R.id.photo_image);

            expandedLayout = (LinearLayout) v.findViewById(R.id.expanded_layout);
            collapsedLayout = (LinearLayout) v.findViewById(R.id.collapsed_layout);
        }
    }

    public interface Listener {
        void onClockClicked(int position);
    }
}
