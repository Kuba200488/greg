package pl.jesion.mykitchen.adapter;

public class ListItem {
    private final String name;
    private final String description;
    private boolean favorite;
    private boolean recent;
    private final long time;
    private long timeLeft;
    private boolean expanded;
    private String imageUrl;

    public ListItem(
            final String name,
            final String description,
            final boolean favorite,
            final boolean recent,
            final long time,
            final String imageUrl
    ) {
        this.name = name;
        this.description = description;
        this.favorite = favorite;
        this.recent = recent;
        this.time = time;
        this.timeLeft = time;
        this.expanded = false;
        this.imageUrl = imageUrl;
    }

    public boolean isRunning() {
        return timeLeft != time;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(final boolean favorite) {
        this.favorite = favorite;
    }

    public boolean isRecent() {
        return recent;
    }

    public void setRecent(final boolean recent) {
        this.recent = recent;
    }

    public long getTime() {
        return time;
    }

    public void setTimeLeft(final long timeLeft) {
        this.timeLeft = timeLeft;
    }

    public long getTimeLeft() {
        return timeLeft;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(final boolean expanded) {
        this.expanded = expanded;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(final String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public synchronized void toggle() {
        if (expanded) {
            expanded = false;
        } else {
            expanded = true;
        }
    }
}
