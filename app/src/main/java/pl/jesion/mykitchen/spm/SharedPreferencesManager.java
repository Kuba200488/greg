package pl.jesion.mykitchen.spm;

import android.content.Context;
import android.content.SharedPreferences;

import pl.jesion.mykitchen.util.Language;

/**
 * Created by kuba on 10.06.2017.
 */

public class SharedPreferencesManager {

    private static final String PREFS_TAG = "PREFS_TAG";

    private static final String KEY_AUTH_TOKEN = "KEY_AUTH_TOKEN";
    private static final String KEY_NAME = "KEY_NAME";
    private static final String KEY_AUTO_LANGUAGE = "KEY_AUTO_LANGUAGE";
    private static final String KEY_LANGUAGE = "KEY_LANGUAGE";

    SharedPreferences sp;
    private static SharedPreferencesManager spm;

    private SharedPreferencesManager(final Context context) {
        sp = context.getApplicationContext().getSharedPreferences(PREFS_TAG, Context.MODE_PRIVATE);
    }

    public static SharedPreferencesManager getInstance(Context context) {
        if (spm == null) {
            spm = new SharedPreferencesManager(context);
        }
        return spm;
    }

    public void setUserToken(final String val) {
        edit().putString(KEY_AUTH_TOKEN, val).apply();
    }

    public String getUserToken() {
        return sp.getString(KEY_AUTH_TOKEN, null);
    }

    public void setUserName(final String val) {
        edit().putString(KEY_NAME, val).apply();
    }

    public String getUserName() {
        return sp.getString(KEY_NAME, null);
    }

    public void setAutoLanguage(final boolean val) {
        edit().putBoolean(KEY_AUTO_LANGUAGE, val).apply();
    }

    public boolean isAutoLanguage() {
        return sp.getBoolean(KEY_AUTO_LANGUAGE, true);
    }

    public void setLanguage(final Language val) {
        edit().putString(KEY_LANGUAGE, val.name()).apply();
    }

    public Language getLanguage() {
        return Language.valueOf(sp.getString(KEY_LANGUAGE, Language.EN.name()));
    }

    private SharedPreferences.Editor edit() {
        return sp.edit();
    }
}
