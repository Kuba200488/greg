package pl.jesion.mykitchen.mock;

import java.util.Arrays;
import java.util.List;

import pl.jesion.mykitchen.adapter.ListItem;

/**
 * Created by kuba on 13.05.2017.
 */

public final class Mock {
    public static List<ListItem> getMocked() {
        return Arrays.asList(
                new ListItem("French Onion Soup", "Put into hot water (80C)", false, true, 120, null),
                new ListItem("Chili pork", "Put into hot water (80C)", true, true, 360, null),
                new ListItem("Pizza", "Put into hot water (80C)", false, false, 20, null),
                new ListItem("Git burger", "Put into hot water (80C)", false, true, 100, null),
                new ListItem("Pad thai", "Put into hot water (80C)", true, false, 300, null),
                new ListItem("Steak with fries", "Put into hot water (80C)", false, false, 540, null),
                new ListItem("Tomato soup", "Put into hot water (80C)", false, true, 120, null),
                new ListItem("Hod dog", "Put into hot water (80C)", false, false, 120, null),
                new ListItem("Stripes", "Put into hot water (80C)", true, true, 630, null),
                new ListItem("Pullet pork", "Put into hot water (80C)", false, false, 240, null),
                new ListItem("Tomato spagetti", "Put into hot water (80C)", false, true, 120, null),
                new ListItem("Hod habanas pizza", "Put into hot water (80C)", false, false, 120, null),
                new ListItem("Gulash", "Put into hot water (80C)", true, true, 630, null),
                new ListItem("Pullet beef", "Put into hot water (80C)", false, false, 240, null)
        );
    }
}
