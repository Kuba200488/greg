package pl.jesion.mykitchen.filter;

import java.util.ArrayList;
import java.util.List;

import pl.jesion.mykitchen.adapter.ListItem;

public final class FilterUtil {

    public static List<ListItem> getRecentItems(List<ListItem> all) {
        List<ListItem> recentItems = new ArrayList<>();
        for (ListItem item : all) {
            if (item.isRecent())
                recentItems.add(item);
        }
        return recentItems;
    }

    public static List<ListItem> getFavoriteItems(List<ListItem> all) {
        List<ListItem> recentItems = new ArrayList<>();
        for (ListItem item : all) {
            if (item.isFavorite())
                recentItems.add(item);
        }
        return recentItems;
    }
}
