package pl.jesion.mykitchen.fragment;

import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.annimon.stream.Stream;

import pl.jesion.mykitchen.MainActivity;
import pl.jesion.mykitchen.R;
import pl.jesion.mykitchen.adapter.ListAdapter;
import pl.jesion.mykitchen.adapter.ListItem;
import pl.jesion.mykitchen.util.MyTimer;

public class ListFragment extends Fragment implements ListAdapter.Listener, MyTimer.Listener, ItemsFragment {

    RecyclerView allList;
    ListAdapter adapter;
    List<ListItem> items;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        allList = (RecyclerView) view.findViewById(R.id.all_list);
        return view;
    }

    @Override
    public void onClockClicked(final int position) {
        ListItem item = items.get(position);
        if (item != null && !item.isRunning()) {
            MyTimer timer = new MyTimer(items.get(position), adapter, this);
            timer.start();
        }
    }

    @Override
    public void onViewCreated(
            final View view,
            @Nullable final Bundle savedInstanceState
    ) {
        super.onViewCreated(view, savedInstanceState);
        refreshList(((MainActivity)getActivity()).getItems());
    }

    @Override
    public void onCountingFinished(final ListItem item) {
        Toast.makeText(getContext(), "Counting for " + item.getName() + " finished!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void refreshList(final List<ListItem> allItems) {
        this.items = Stream.ofNullable(allItems)
                .withoutNulls()
                .toList();

        if (allList != null) {
            adapter = new ListAdapter(getContext(), items, this);
            allList.setAdapter(adapter);
            allList.setLayoutManager(new LinearLayoutManager(getContext()));
        }
    }
}
