package pl.jesion.mykitchen.fragment;

import java.util.List;

import pl.jesion.mykitchen.adapter.ListItem;

/**
 * Created by kuba on 11.06.2017.
 */

public interface ItemsFragment {
    void refreshList(List<ListItem> items);
}
