package pl.jesion.mykitchen.fragment;

import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;

import pl.jesion.mykitchen.R;
import pl.jesion.mykitchen.adapter.ListAdapter;
import pl.jesion.mykitchen.adapter.ListItem;
import pl.jesion.mykitchen.filter.FilterUtil;
import pl.jesion.mykitchen.mock.Mock;

public class FavoritesFragment extends Fragment implements ListAdapter.Listener, ItemsFragment {

    RecyclerView favoritesList;
    ListAdapter adapter;
    List<ListItem> items;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);

        favoritesList = (RecyclerView) view.findViewById(R.id.favorites_list);

        return view;
    }

    @Override
    public void onClockClicked(final int position) {

    }

    @Override
    public void refreshList(final List<ListItem> allItems) {
        this.items = Stream.ofNullable(allItems)
                .withoutNulls()
                .filter(new Predicate<ListItem>() {
                    @Override
                    public boolean test(final ListItem item) {return item.isFavorite();}
                })
                .toList();

        if (favoritesList != null) {
            adapter = new ListAdapter(getContext(), items, this);
            favoritesList.setAdapter(adapter);
            favoritesList.setLayoutManager(new LinearLayoutManager(getContext()));
        }
    }
}
