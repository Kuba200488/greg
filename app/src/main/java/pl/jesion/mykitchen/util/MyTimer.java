package pl.jesion.mykitchen.util;

import android.os.CountDownTimer;

import pl.jesion.mykitchen.adapter.ListAdapter;
import pl.jesion.mykitchen.adapter.ListItem;

public class MyTimer extends CountDownTimer {

    private static final int MILLIS_IN_SEC = 1000;
    private Listener listener;
    private ListItem item;
    private ListAdapter adapter;
    private boolean firstTick;

    public MyTimer(ListItem item, ListAdapter adapter, Listener listener) {
        super(item.getTime() * MILLIS_IN_SEC, MILLIS_IN_SEC);
        this.item = item;
        this.adapter = adapter;
        this.listener = listener;
        firstTick = true;
    }

    @Override
    public void onTick(final long millisUntilFinished) {
        if (firstTick) {
            firstTick = false;
        } else {
            item.setTimeLeft(item.getTimeLeft() - 1);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFinish() {
        item.setTimeLeft(0);
        adapter.notifyDataSetChanged();
        listener.onCountingFinished(item);
    }

    public interface Listener {
        void onCountingFinished(ListItem item);
    }
}
