package pl.jesion.mykitchen.util;

public final class TimeUtil {
    public static String getFormatedTime(long seconds) {
        long minutes = seconds / 60;
        seconds = seconds % 60;
        return minutes + ":" + seconds;
    }
}
