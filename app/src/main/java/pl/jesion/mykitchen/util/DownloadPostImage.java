package pl.jesion.mykitchen.util;

import java.io.InputStream;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

/**
 * Created by kuba on 11.06.2017.
 */

public class DownloadPostImage extends AsyncTask<String, Void, Bitmap> {
    ImageView imageView;

    public DownloadPostImage(ImageView imageView) {
        this.imageView = imageView;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    protected Bitmap doInBackground(String... urls) {
        if (urls[0] == null) {
            return null;
        }
        Log.d("PhotoArrayAdapter", "Downloading...");
        String imageUrl = urls[0];
        Bitmap imageBitmap = null;
        try {
            Log.d("URL : ", imageUrl);
            InputStream in = new java.net.URL(imageUrl).openStream();
            imageBitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            imageBitmap = null;
            e.printStackTrace();
        }
        return imageBitmap;
    }

    protected void onPostExecute(Bitmap result) {
        Log.d("PhotoArrayAdapter", "Downloaded photo");

        if (imageView != null) {
            if (result != null) {
                imageView.setImageBitmap(result);
            } else {
                imageView.setImageBitmap(null);
            }
        }
    }
}