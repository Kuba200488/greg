package pl.jesion.mykitchen;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.jesion.mykitchen.adapter.ListItem;
import pl.jesion.mykitchen.fragment.FavoritesFragment;
import pl.jesion.mykitchen.fragment.HomeFragment;
import pl.jesion.mykitchen.fragment.ItemsFragment;
import pl.jesion.mykitchen.fragment.ListFragment;
import pl.jesion.mykitchen.network.rest.AddFoodRequest;
import pl.jesion.mykitchen.network.rest.GedFoodsRequest;
import pl.jesion.mykitchen.network.rest.LoginRequest;
import pl.jesion.mykitchen.network.rest.RegisterRequest;
import pl.jesion.mykitchen.spm.SharedPreferencesManager;
import pl.jesion.mykitchen.util.Language;

import static pl.jesion.mykitchen.util.Language.EN;
import static pl.jesion.mykitchen.util.Language.PL;

public class MainActivity extends AppCompatActivity implements LocationListener {

    private LocationManager locationManager;

    private static final int IMAGE_REQUEST = 45;

    private static final int ITEMS_COUNT = 3;
    private ViewPager viewPager;
    private BottomNavigationView navigation;
    private ImageButton settingsButton;
    private FloatingActionButton addButton;
    private Toast toast;

    private ImageView addPhotoImageView = null;
    private Bitmap addPhotoBitmap = null;

    private List<ItemsFragment> fragments;

    private List<ListItem> items = Collections.emptyList();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    viewPager.setCurrentItem(0);
                    return true;
                case R.id.navigation_dashboard:
                    viewPager.setCurrentItem(1);
                    return true;
                case R.id.navigation_notifications:
                    viewPager.setCurrentItem(2);
                    return true;
                default:
                    break;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        viewPager = (ViewPager) findViewById(R.id.content_pager);
        fragments = new ArrayList<>();
        fragments.addAll(Arrays.asList(
                new HomeFragment(),
                new FavoritesFragment(),
                new ListFragment()
        ));
        setupViewPager(viewPager);
        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        settingsButton = (ImageButton) findViewById(R.id.settings_button);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                openSettings();
            }
        });
        addButton = (FloatingActionButton) findViewById(R.id.add_fab);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                openAddDialog();
            }
        });
        new CountDownTimer(1000, 1000) {
            @Override
            public void onTick(final long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                fetchFoods();
            }
        }.start();

        Resources res = this.getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        conf.setLocale(new Locale(getSpm().getLanguage().name().toLowerCase()));
        res.updateConfiguration(conf, dm);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                60*60*1000,
                10000, this
        );
    }

    public List<ListItem> getItems() {
        return items;
    }

    public void fetchFoods() {
        Volley.newRequestQueue(this).add(
                new GedFoodsRequest(
                        new GedFoodsRequest.Listener() {
                            @Override
                            public void onSuccess(JSONArray array) {
                                showMessage("Foods fetching completed");
                                for (ItemsFragment itemsFragment : fragments) {
                                    items = getItems(array);
                                    itemsFragment.refreshList(getItems(array));
                                }
                            }

                            @Override
                            public void onError(final Error error) {
                                showMessage("Error fetching foods. Please log in");
                            }
                        },
                        getSpm().getUserToken()
                )
        );
    }

    private void getLanguage(Location location) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (getSpm().isAutoLanguage() && !addresses.isEmpty()) {
                Address obj = addresses.get(0);
                if (obj.getCountryCode().equals("pl")) {
                    setLanguage(EN);
                } else {
                    setLanguage(PL);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(final Location location) {
        getLanguage(location);
    }

    @Override
    public void onStatusChanged(
            final String provider,
            final int status,
            final Bundle extras
    ) {

    }

    @Override
    public void onProviderEnabled(final String provider) {

    }

    @Override
    public void onProviderDisabled(final String provider) {

    }

    private List<ListItem> getItems(final JSONArray array) {
        List<ListItem> items = new ArrayList<>();
        for (int i = 0 ; i < array.length() ; i++) {
            try {
                items.add(parseJson(array.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return items;
    }

    private ListItem parseJson(JSONObject object) {
        try {
            return new ListItem(
                    object.getString("name"),
                    object.getString("description"),
                    object.getBoolean("is_favourite"),
                    true,
                    object.getLong("cooking_time"),
                    object.getString("image_url")
            );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void openAddDialog() {
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_add, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();

        final EditText nameTV = (EditText) dialogView.findViewById(R.id.name);
        final EditText descriptionTV = (EditText) dialogView.findViewById(R.id.description);
        final TimePicker timePicker = (TimePicker) dialogView.findViewById(R.id.time_picker);
        final ImageView photoImage = (ImageView) dialogView.findViewById(R.id.image_select);
        final Button saveButton = (Button) dialogView.findViewById(R.id.save_btn);


        photoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                openCameraForImage(photoImage);
            }
        });
        timePicker.setIs24HourView(true);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {

                AddFoodRequest req = new AddFoodRequest(
                        AddFoodRequest.createBodyJson(
                                nameTV.getText().toString(),
                                descriptionTV.getText().toString(),
                                (timePicker.getHour()*60 + timePicker.getMinute())*60,
                                addPhotoBitmap
                        ),
                        new AddFoodRequest.Listener() {
                            @Override
                            public void onSuccess() {
                                showMessage("Dish saved");
                                fetchFoods();
                                dialog.dismiss();
                            }

                            @Override
                            public void onError(final Error error) {
                                showMessage("Error creating dish");
                            }
                        },
                        getSpm().getUserToken()
                );

                Log.d("REQUEST:", req.toString());

                Volley.newRequestQueue(MainActivity.this).add(req);
            }
        });

        dialog.show();
    }

    private void openCameraForImage(final ImageView imageView) {
        addPhotoImageView = imageView;
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(
            final int requestCode,
            final int resultCode,
            final Intent data
    ) {
        super.onActivityResult(requestCode, resultCode, data);
        if (addPhotoImageView != null && requestCode == IMAGE_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            addPhotoBitmap = photo;
            addPhotoImageView.setImageBitmap(photo);
        }
    }

    private void openSettings() {
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_settings, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();

        final CheckBox autoLanguageCbx = (CheckBox) dialogView.findViewById(R.id.auto_language_select);
        final RadioGroup languagRadioGroup = (RadioGroup) dialogView.findViewById(R.id.language_radio_group);
        final Button loginButton = (Button) dialogView.findViewById(R.id.login);

        loginButton.setText(isUserLogged() ? "Logout" : "Login/Register");

        autoLanguageCbx.setChecked(getSpm().isAutoLanguage());
        autoLanguageCbx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(
                    final CompoundButton buttonView,
                    final boolean isChecked
            ) {
                getSpm().setAutoLanguage(isChecked);
                languagRadioGroup.setVisibility(getSpm().isAutoLanguage() ? View.GONE : View.VISIBLE);
            }
        });

        languagRadioGroup.check(getSpm().getLanguage().equals(EN) ? R.id.language_en : R.id.language_pl);

        languagRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(
                    final RadioGroup group,
                    @IdRes final int checkedId
            ) {
                setLanguage(checkedId == R.id.language_en ? EN : PL);
            }
        });

        languagRadioGroup.setVisibility(getSpm().isAutoLanguage() ? View.GONE : View.VISIBLE);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (isUserLogged()) {
                    getSpm().setUserToken(null);
                    dialog.dismiss();
                } else {
                    openAuthenticationDialog();
                    dialog.dismiss();
                }
            }
        });

        dialog.show();
    }

    private void setLanguage(final Language language) {
        getSpm().setLanguage(language);
    }

    private void openAuthenticationDialog() {

        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_authenticate, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        final AlertDialog dialog = builder.create();

        final EditText loginEmail = (EditText) dialogView.findViewById(R.id.login_email);
        final EditText loginPassword = (EditText) dialogView.findViewById(R.id.login_password);
        final Button loginButton = (Button) dialogView.findViewById(R.id.login_btn);

        final EditText registerEmail = (EditText) dialogView.findViewById(R.id.register_email);
        final EditText registerNick = (EditText) dialogView.findViewById(R.id.register_name);
        final EditText registerPassword = (EditText) dialogView.findViewById(R.id.register_password);
        final Button registerButton = (Button) dialogView.findViewById(R.id.register_btn);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (loginEmail.getText().toString().isEmpty()
                        || loginPassword.getText().toString().isEmpty()) {
                    showMessage("Fill all login fields");
                    return;
                }
                showMessage("Please wait");
                Volley.newRequestQueue(MainActivity.this).add(
                        new LoginRequest(
                                LoginRequest.createBodyJson(
                                        loginEmail.getText().toString(),
                                        loginPassword.getText().toString()
                                ),
                                new LoginRequest.Listener() {
                                    @Override
                                    public void onLoginSuccess(final String authToken) {
                                        Log.d("AUTH_TOKEN",authToken);
                                        showMessage("Login succeeded");
                                        setUserToken(authToken);
                                        dialog.dismiss();
                                        fetchFoods();
                                    }

                                    @Override
                                    public void onError(final Error error) {
                                        showMessage("Login failed, check your connection and credentials");
                                    }
                                }
                        )
                );
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (registerEmail.getText().toString().isEmpty()
                        || registerNick.getText().toString().isEmpty()
                        || registerPassword.getText().toString().isEmpty()) {
                    showMessage("Fill all register fields");
                    return;
                }
                showMessage("Please wait");
                Volley.newRequestQueue(MainActivity.this).add(
                        new RegisterRequest(
                                RegisterRequest.createBodyJson(
                                        registerNick.getText().toString(),
                                        registerEmail.getText().toString(),
                                        registerPassword.getText().toString()),
                                new RegisterRequest.Listener() {
                                    @Override
                                    public void onRegisterSuccess(final String authToken) {
                                        showMessage("Registration succeeded");
                                        setUserToken(authToken);
                                        dialog.dismiss();
                                    }

                                    @Override
                                    public void onError(final Error error) {
                                        showMessage("Registration failed, check your connection and credentials");
                                    }
                                }
                        )
                );
            }
        });

        dialog.show();
    }

    private boolean isUserLogged() {
        return getSpm().getUserToken() != null;
    }

    private void showMessage(final String message) {
        if (toast != null) {
            toast.cancel();
        }
        toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.show();
    }

    private void setUserToken(final String token) {
        getSpm().setUserToken(token);
    }

    private void setupViewPager(ViewPager viewPager) {
        List<Fragment> frags = new ArrayList<>();
        for (ItemsFragment itemsFragment : fragments) {
            frags.add((Fragment)itemsFragment);
        }
        MyAdapter adapter = new MyAdapter(getSupportFragmentManager(), frags);
        viewPager.setAdapter(adapter);
    }

    private static class MyAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;

        private MyAdapter(
                FragmentManager fm,
                List<Fragment> fragments
        ) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }
    }

    private SharedPreferencesManager getSpm() {
        return SharedPreferencesManager.getInstance(this);
    }
}
