package pl.jesion.mykitchen.network.rest;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import android.graphics.Bitmap;
import android.util.Base64;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

/**
 * Created by kuba on 10.06.2017.
 */

public class AddFoodRequest extends JsonObjectRequest {

    private Listener listener;
    private String authToken;

    public AddFoodRequest(
            final JSONObject bodyJson,
            final Listener listener,
            final String authToken

    ) {
        super(
                Method.POST,
                RequestUrls.FOOD_URL,
                bodyJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        listener.onSuccess();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        listener.onError(new Error(error.getMessage()));
                        error.printStackTrace();
                    }
                }
        );
        this.authToken = authToken;
    }

    public static JSONObject createBodyJson(
            final String name,
            final String description,
            final long cookingTime,
            final Bitmap bitmap
    ) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        params.put("description", description);
        params.put("cooking_time", String.valueOf(cookingTime));
        params.put("is_favourite", "false");
        params.put("image", encodeTobase64(bitmap));


        JSONObject obj = new JSONObject(params);

        Log.d("materialy", obj.toString());

        return obj;
    }

    @Override
    public byte[] getBody() {
        return super.getBody();
    }

    public interface Listener {
        void onSuccess();
        void onError(Error error);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Authorization", authToken);
        return params;
    }

    public static String encodeTobase64(Bitmap bm)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG,100,baos);
        byte[] b = baos.toByteArray();
        return "data:image/gif;base64," + Base64.encodeToString(b, Base64.DEFAULT);
    }
}