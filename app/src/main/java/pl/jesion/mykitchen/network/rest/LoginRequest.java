package pl.jesion.mykitchen.network.rest;

import java.util.HashMap;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import static pl.jesion.mykitchen.network.rest.RegisterRequest.KEY_TOKEN;

public class LoginRequest extends JsonObjectRequest {

    private Listener listener;

    public LoginRequest(
            final JSONObject bodyJson,
            final Listener listener
    ) {
        super(
                Method.POST,
                RequestUrls.LOGIN_URL,
                bodyJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            listener.onLoginSuccess(response.getString(KEY_TOKEN));
                        } catch (JSONException e) {
                            listener.onError(new Error(e.getMessage()));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        listener.onError(new Error(error.getMessage()));
                    }
                }
        );
    }

    public static JSONObject createBodyJson(
            final String email,
            final String password
    ) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", email);
        params.put("password", password);
        return new JSONObject(params);
    }

    @Override
    public byte[] getBody() {
        return super.getBody();
    }

    public interface Listener {
        void onLoginSuccess(final String authToken);
        void onError(Error error);
    }
}
