package pl.jesion.mykitchen.network.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import pl.jesion.mykitchen.adapter.ListItem;

/**
 * Created by kuba on 11.06.2017.
 */

public class GedFoodsRequest extends JsonArrayRequest {

    private Listener listener;
    private String authToken;

    public GedFoodsRequest(
            final Listener listener,
            final String authToken
    ) {
        super(
                Method.GET,
                RequestUrls.FOOD_URL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(final JSONArray response) {
                        listener.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        listener.onError(new Error(error.getMessage()));
                        error.printStackTrace();
                    }
                }
        );
        this.authToken = authToken;
    }

    @Override
    public byte[] getBody() {
        return super.getBody();
    }

    public interface Listener {
        void onSuccess(JSONArray items);
        void onError(Error error);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Authorization", authToken);
        return params;
    }
}