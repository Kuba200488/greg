package pl.jesion.mykitchen.network.rest;

public final class RequestUrls {

    private static final String SERVER_URL = "https://food-timer-client-api.herokuapp.com/";

    public static final String REGISTER_URL = SERVER_URL + "signup";
    public static final String LOGIN_URL = SERVER_URL + "auth/login";
    public static final String FOOD_URL = SERVER_URL + "foods";
}
