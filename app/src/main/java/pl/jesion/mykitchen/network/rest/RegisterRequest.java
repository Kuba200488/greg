package pl.jesion.mykitchen.network.rest;

import java.util.HashMap;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

public class RegisterRequest extends JsonObjectRequest {

    public static final String KEY_TOKEN = "auth_token";

    private Listener listener;

    public RegisterRequest(
            final JSONObject bodyJson,
            final Listener listener
    ) {
        super(
                Method.POST,
                RequestUrls.REGISTER_URL,
                bodyJson,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        try {
                            listener.onRegisterSuccess(response.getString(KEY_TOKEN));
                        } catch (JSONException e) {
                            listener.onError(new Error(e.getMessage()));
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(final VolleyError error) {
                        listener.onError(new Error(error.getMessage()));
                    }
                }
        );
    }

    public static JSONObject createBodyJson(
            final String name,
            final String email,
            final String password
    ) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        params.put("email", email);
        params.put("password", password);
        return new JSONObject(params);
    }

    @Override
    public byte[] getBody() {
        return super.getBody();
    }

    public interface Listener {
        void onRegisterSuccess(final String authToken);
        void onError(Error error);
    }
}
